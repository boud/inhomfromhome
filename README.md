# Inhomogeneous cosmologies online


Cosmologists have recently organised several online meetings
([Cosmology from home][1], [GR sims in cosmology][2]) that are quite
embarrassingly dependent on [GAFAM][3] and create new dependencies on
non-free software and on new centralised, authoritarian corporations
([Zoom/Slack][4]) that violate the principles of participatory, rational,
informed, transparent decision-making and communication, in opposition
to the widely held ethical principles.

This git repository aims to form a key element of organising an
online inhomogeneous cosmology meeting during the COVID-19 epoch,
and provide an example for scientists in other fields who may wish
to hold similar types of online meetings. Feel free to fork this
git repository and/or add 'issues' or propose 'pull requests'.

Known examples of ethical science/software online meetings include:
* [DebConf20][DebC20] using "Jitsi, OBS, Voctomix, SReview, nginx, Etherpad" at [ansible][ansible];
* [Libre Planet 2020][LibreP20] using jitsi, gstreamer, icecast, irc;
* [Journées nationales du DEVeloppement 2020][JDEV20] using [bbb and other free tools][Civodul20];
* [Free and open source software conference 2020][Froscon20] using [bbb][Heurekus20];
* [Art Meets Radical Openness 2020][AMRO20] using [bbb+etherpad+peertube][Fionnain20];

Near future:
* [Vintage Computing Festival Berlin 2020][VCFB20] using [wiki+bbb][Heurekus20];
* [FOSDEM 2021][FOSDEM21].

Known software and services that are likely to be useful are listed at
[switching.software][swiso] and [reverseeagle.org][reveag].


[1]: https://www.cosmologyfromhome.com/
[2]: https://www.qmul.ac.uk/spa/astro/events/gr-simulations-in-cosmology/
[3]: https://en.wikipedia.org/wiki/GAFAM
[4]: https://cosmo.torun.pl/blog/slack_zoom_prison 

[DebC20]: https://debconf20.debconf.org/
[ansible]: https://salsa.debian.org/debconf-video-team/ansible
[LibreP20]: https://www.fsf.org/blogs/community/looking-back-at-libreplanet-2020-freeing-the-future-together
[JDEV20]: http://devlog.cnrs.fr/jdev2020
[Civodul20]: https://toot.aquilenet.fr/@civodul/104802053012374231
[Froscon20]: https://www.froscon.de/en/
[Heurekus20]: https://mastodon.social/@heurekus/104805074529936431
[AMRO20]: https://gateway.radical-openness.org/
[Fionnain20]: https://mograph.social/@ephemeral/104805514229531250

[VCFB20]: https://wiki.vcfb.de/2020/en:start
[FOSDEM21]: https://fosdem.org/2021/

[swiso]: https://switching.software
[reveag]: https://developers.reverseeagle.org/


(C) Copyright CC-BY B.Roukema 2020.
You are free to share and adapt this file within the conditions
of the CC-BY licence: https://creativecommons.org/licenses/by/4.0
